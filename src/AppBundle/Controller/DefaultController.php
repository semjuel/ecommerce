<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Helper\CsvParser;

/**
 * @Route("/category")
 */
class DefaultController extends Controller
{
    use CsvParser;

    const DEFAULT_PER_PAGE = 9;

    /**
     * @Route("/{id}.html", name="show_category")
     * @Template()
     */
    public function indexAction(Request $request, Category $category)
    {
        $limit = $request->get('limit', self::DEFAULT_PER_PAGE);

        $doctrine = $this->container->get('doctrine');
        $repo = $doctrine->getRepository('AppBundle:Category');
        $category = $repo->find($category);

        $doctrine = $this->container->get('doctrine');
        $repo = $doctrine->getRepository('AppBundle:Product');
        $products = $repo->findActiveProductsByCategory($category, $limit);

        return $this->render('AppBundle:Default:index.html.twig', ['products' => $products]);
        //return ['products' => $products];
        //return new \Symfony\Component\HttpFoundation\Response();

    }

}
