<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 * @Route("/feedback")
 */
class FeedbackController extends Controller
{
    const PER_PAGE = 10;

    /**
     * @Route("/", name="feedback")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $feedback = new Feedback();
        $form = $this->createForm(new FeedbackType(), $feedback);
        //$form = $this->createForm(new FeedbackType(), new Feedback());
        $form->add('submit', 'submit');
        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $doctrine = $this->container->get('doctrine')->getManager();
                $doctrine->persist($form->getData());
                $doctrine->flush();
                // flushBack - тут закинуть и в твиге вывести сообщение
                return new RedirectResponse($this->generateUrl('show_category', array('id' => 34)));
            }
        }

        return ['form' => $form->createView()];
    }
}
