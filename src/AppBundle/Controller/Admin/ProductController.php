<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\Admin\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/product")
 */
class ProductController extends Controller
{
  /**
   * @Route("/list.html", name="show_admin_products")
   * @Template()
   */
  public function indexAction()
  {
    $doctrine = $this->container->get('doctrine');
    $repo = $doctrine->getRepository('AppBundle:Product');
    $products = $repo->getAllProducts();

    return $this->render('AppBundle:Admin:Product/index.html.twig', ['products' => $products]);
  }

  /**
   * @Route("/add.html", name="create_product")
   * @Template("AppBundle:Admin:Product/create.html.twig")
   */
  public function createProduct(Request $request)
  {
    $product = new Product(time());
    $form = $this->createForm(new ProductType(), $product);
    $form->add('submit', 'submit');

    if ($request->isMethod(Request::METHOD_POST)) {
      $form->handleRequest($request);
      if ($form->isValid()) {
        $doctrine = $this->container->get('doctrine')->getManager();
        $doctrine->persist($form->getData());
        $doctrine->flush();
        // flushBack - тут закинуть и в твиге вывести сообщение
        return new RedirectResponse($this->generateUrl('/admin/product/list.html', array('id' => 34)));
      }
    }

    return ['form' => $form->createView()];
  }
}