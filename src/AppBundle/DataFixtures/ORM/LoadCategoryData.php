<?php

namespace AppBundle\DataFixture;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Helper\CsvParser;

class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    use CsvParser;

    /**
     * @param ObjectManager $manager
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'category.csv';
        $csvData = self::getCsvData($filePath);

        foreach($csvData as $categoryRow) {
            $category = (new Category())
                ->setTitle($categoryRow[0]);
            // Check if category is active.
            if ($categoryRow[1]) {
                $category->enabled();
            }
            else {
                $category->disabled();
            }
            $manager->persist($category);
            $this->setReference($categoryRow[2], $category);
        }

        $manager->flush();
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return 150;
    }


}