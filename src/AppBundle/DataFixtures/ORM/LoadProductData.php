<?php

namespace AppBundle\DataFixture;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Helper\CsvParser;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    use CsvParser;

    /**
     * @param ObjectManager $manager
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'product.csv';
        $csvData = self::getCsvData($filePath);

        foreach($csvData as $productRow) {
            /** @var Category $category */
            $category = $this->getReference($productRow[5]);
            $product = (new Product($productRow[2]))
                ->setTitle($productRow[0])
                ->setPrice($productRow[3])
                ->setDescription($productRow[4])
                ->setImage($productRow[6])
                ->setCategory($category);
            if ($productRow[1]) {
                $product->enabled();
            }
            else {
                $product->disabled();
            }
            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return 250;
    }
}