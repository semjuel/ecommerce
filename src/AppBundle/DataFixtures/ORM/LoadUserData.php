<?php

namespace AppBundle\DataFixture;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use AppBundle\Helper\CsvParser;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    use CsvParser;

    /**
     * @param ObjectManager $manager
     * @return mixed
     */
    public function load(ObjectManager $manager)
    {
        $filePath = __DIR__ . DIRECTORY_SEPARATOR . 'user.csv';
        $csvData = self::getCsvData($filePath);

        foreach($csvData as $productRow) {
            $user = (new User())
                ->setEmail($productRow[0])
                ->setPass($productRow[1]);
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return 350;
    }
}