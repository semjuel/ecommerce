<?php

namespace AppBundle\Entity;


trait ActiveAwareTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * Set product enabled
     *
     * @return Object
     */
    public function enabled()
    {
        $this->active = true;

        return $this;
    }

    /**
     * Set product disabled
     *
     * @return Object
     */
    public function disabled()
    {
        $this->active = false;

        return $this;
    }


    /**
     * Get active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }
}