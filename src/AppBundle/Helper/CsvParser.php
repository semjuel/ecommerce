<?php

namespace AppBundle\Helper;

trait CsvParser
{
    public function getCsvData($filePath)
    {
        $csvData = array();

        if($handle = fopen($filePath, 'r')) {
            while ($line = fgetcsv($handle, 4096, ';')) {
                $csvData[] = $line;
            }
        }

        return $csvData;
    }
}