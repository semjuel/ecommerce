<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\DBAL\LockMode;

/**
 * CategoryRepository
 *
 */
class CategoryRepository extends EntityRepository
{
    public function find($id, $lockMode = LockMode::NONE, $lockVersion = null)
    {
        $category = parent::find($id, $lockMode, $lockVersion);

        if (is_null($category)) {
            throw new EntityNotFoundException();
        }

        return $category;
    }
}
